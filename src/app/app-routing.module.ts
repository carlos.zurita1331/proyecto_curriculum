import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaComponent } from './components/agenda/agenda.component';
import { CurriculumVComponent } from './components/curriculum-v/curriculum-v.component';

import { FormComponent } from './components/form/form.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {path:'home',component:HomeComponent},
  {path:'curriculum',component:CurriculumVComponent},
  {path:'agenda',component:AgendaComponent},
  {path:'form',component:FormComponent},
  { path: '**', pathMatch:'full',redirectTo: 'home' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
