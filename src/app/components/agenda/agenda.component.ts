import {Component, OnInit} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {Observable, ReplaySubject} from 'rxjs';


export interface PeriodicElement {

  nombres:string;
  apellidos:string;
  email:string;
  celular:number;
  fecha :Number;
  hora:String;
  descripcion:string;  
}

const ELEMENT_DATA: PeriodicElement[] = [
  {nombres: 'marina rosa', apellidos: 'Zambrana Delgado', email: 'marinar@gmail.com', celular: 67598521 ,fecha:12/2/21,hora:'4:42 PM',descripcion: 'reunion'},
  {nombres: 'marina rosa', apellidos: 'Zambrana Delgado', email: 'marinar@gmail.com', celular: 67598521 ,fecha:12/2/21,hora:'4:42 PM',descripcion: 'reunion'},
  {nombres: 'marina rosa', apellidos: 'Zambrana Delgado', email: 'marinar@gmail.com', celular: 67598521 ,fecha:12/2/21,hora:'4:42 PM',descripcion: 'reunion'},
  {nombres: 'marina rosa', apellidos: 'Zambrana Delgado', email: 'marinar@gmail.com', celular: 67598521 ,fecha:12/2/21,hora:'4:42 PM',descripcion: 'reunion'},
  {nombres: 'marina rosa', apellidos: 'Zambrana Delgado', email: 'marinar@gmail.com', celular: 67598521 ,fecha:12/2/21,hora:'4:42 PM',descripcion: 'reunion'},

];


@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})

export class AgendaComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  displayedColumns: string[] = ['nombres', 'apellidos', 'email','celular','fecha','hora','descripcion'];
  dataToDisplay = [...ELEMENT_DATA];

  dataSource = new ExampleDataSource(this.dataToDisplay);

  addData() {
    const randomElementIndex = Math.floor(Math.random() * ELEMENT_DATA.length);
    this.dataToDisplay = [...this.dataToDisplay, ELEMENT_DATA[randomElementIndex]];
    this.dataSource.setData(this.dataToDisplay);
  }

  removeData() {
    this.dataToDisplay = this.dataToDisplay.slice(0, -1);
    this.dataSource.setData(this.dataToDisplay);
  }
}

class ExampleDataSource extends DataSource <PeriodicElement> {
  private _dataStream = new ReplaySubject<PeriodicElement[]>();



  constructor(initialData: PeriodicElement[]) { 
    super();
    this.setData(initialData);
  }

  connect(): Observable<PeriodicElement[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: PeriodicElement[]) {
    this._dataStream.next(data);
  }


}
