import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SharedModule } from './components/shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { CurriculumVComponent } from './components/curriculum-v/curriculum-v.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { FormComponent } from './components/form/form.component';

// angular material




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CurriculumVComponent,
    AgendaComponent,
    FormComponent,

    

  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
    
   
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
